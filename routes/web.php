<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post("/POST/login","UsersController@login");
$router->post("/**POST/users","UsersController@add");
$router->put("/**PUT/users/{id}","UsersController@edit");
$router->post("/**GET/users[/{id}]","UsersController@list");
$router->delete("/**DELETE/users/{id}","UsersController@delete");
$router->get("/token",function(){
    return str_random(60);
});



