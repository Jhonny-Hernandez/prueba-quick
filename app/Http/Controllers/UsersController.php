<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends BaseController
{
    public function login(Request $request){
        
        $response = User::where("email",$request->email)->first();

        if($response && Hash::check($request->password, $response->password))
            return response()->json($response, 200);
        else 
            return response()->json(["Error"=>"Error in user or password"], 404);
            
    }

    public function add(Request $request){
        User::create([
            'email'=>$request->email,
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'token'=>\str_random(60),
            "password"=>Hash::make($request->password)
        ]);
        return response()->json([], 201);
    }

    public function edit($id=null,Request $request){
        if($id!=null)
        {
            User::find($id)->update([
                'email'=>$request->email,
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                "password"=>Hash::make($request->password)
            ]);
            return response()->json([], 204);    
        }
        return response()->json([], 404);
    }

    public function list($id=null){
        if($id==null)
            return response()->json(User::all(), 200);
        else
            return response()->json(User::find($id), 200);
    }

    public function delete($id=null){
        if($id!=null)
        {
            User::destroy($id);
            return response()->json([], 204);    
        }
        return response()->json([], 404);
    }
}
