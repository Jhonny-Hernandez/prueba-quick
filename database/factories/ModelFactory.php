<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'email'=>$faker.email,
        'first_name'=>$faker.first_name,
        'last_name'=>$faker.last_name,
        'token'=>str_random(32),
        'password'=>Crypt::encrypt('secret')
    ];
});
